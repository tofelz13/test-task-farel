from hamcrest import assert_that, equal_to
from pytest import mark

from common.constants import Result


class TestCulculate:
    @mark.parametrize(
        "test_input, expected",
        [
            (0, "1"),
            (1, "1"),
            (5, "120"),
            (170, "7.257415615307999e+306"),
            (978, "Infinity"),
        ],
    )
    def test_calculate_factorial(self, app, test_input, expected):
        """
        1. Open the calculator
        2. Enter a number
        3. Check that result is factorial of number
        """
        app.open_main_page()
        app.main_page.calculate(test_input)
        assert_that(app.main_page.result(), equal_to(expected))

    @mark.parametrize(
        "test_input, expected",
        [
            ("one", Result.ERROR),
            (" ", Result.ERROR),
            ("", Result.ERROR),
            ("12 '", Result.ERROR),
            (")(*&^%$#@", Result.ERROR),
        ],
    )
    def test_incorrect_input(self, app, test_input, expected):
        """
        1. Open the calculator
        2. Enter a incorrect data
        3. Check that there is error message 
        """
        app.open_main_page()
        app.main_page.calculate(test_input)
        assert_that(app.main_page.error_result(), equal_to(expected))
