import logging
from common.constants import Result
from locators.main_page import MainPageLocators
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

logger = logging.getLogger()


class MainPage:
    def __init__(self, app):
        self.app = app

    def get_title(self):
        return self.app.driver.find_element(*MainPageLocators.TITLE)

    def calculate(self, number):
        self.app.driver.find_element(*MainPageLocators.NUMBER_FIELD).send_keys(
            str(number)
        )
        self.app.driver.find_element(*MainPageLocators.CALCULATE).click()

    def result(self):
        wait = WebDriverWait(self.app.driver, 3)
        wait.until(
            ec.text_to_be_present_in_element(
                MainPageLocators.RESULT, Result.RESULT_IS
            )
        )
        return self.app.driver.find_element(*MainPageLocators.RESULT).text.split()[-1]


    def error_result(self):
        wait = WebDriverWait(self.app.driver, 3)
        wait.until(
            ec.text_to_be_present_in_element(
                MainPageLocators.RESULT, Result.ERROR
            )
        )
        return self.app.driver.find_element(*MainPageLocators.RESULT).text
        