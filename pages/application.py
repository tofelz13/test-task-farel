from selenium import webdriver
import logging

from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from pages.main_page import MainPage

logger = logging.getLogger()


class Application:
    def __init__(self, headless, url):
        options: Options = Options()
        if headless:
            options.add_argument("--headless")
        self.url = url
        try:
            self.driver = webdriver.Chrome(
                ChromeDriverManager().install(), options=options
            )
        except ValueError:
            self.driver = webdriver.Chrome(
                r"C:\chromedriver.exe", options=options
            )
        self.driver.implicitly_wait(10)
        self.main_page = MainPage(self)

    def open_main_page(self):
        logger.info("Open main page")
        self.driver.get(self.url)

    def browser_close(self):
        self.driver.quit()
