from selenium.webdriver.common.by import By


class MainPageLocators:
    NUMBER_FIELD = (By.CSS_SELECTOR, '[id="number"]')
    CALCULATE = (By.CSS_SELECTOR, '[id="getFactorial"]')
    RESULT = (By.CSS_SELECTOR, '[id="resultDiv"]')
    PRIVACY = (By.CSS_SELECTOR, '[href="/privacy"]')
