.PHONY: help venv run test

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  clean              => to clean clean all automatically generated files"
	@echo "  venv               => to create a virtualenv"
	@echo "  test               => to run tests"
	@echo "  report             => to generate static html with test results"
	@echo "  all                => clean venv test report"

all: clean venv test report

clean:
	@find . -name \*.pyc -delete
	@find . -name \*__pycache__ -delete
	@find . -name \.pytest_cache -exec rm -r {} +
	@rm -rf allure_results || true
	@rm -rf allure_report || true
	@rm -f junit.xml || true
	@rm -rf ./dist || true
	@rm -rf venv || true

venv:
	@python3 -m venv venv
	@venv/bin/pip install -U -r requirements.txt --disable-pip-version-check

test:
	@venv/bin/python -W ignore -m pytest tests --alluredir=allure_results

report:
	@allure generate allure_results -o allure_report --clean
	@allure open allure_report
