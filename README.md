# Test model
[Test model](Test-model.pdf)

# Requirements
 * Python 3.6 or high
 * Allure 2.7.0


# Getting started

#### 1.Download source

```bash
$ git clone git@gitlab.com:tofelz13/test-task-farel.git
```
#### 2. Make venv

```bash
$ make venv
```

#### 3. Run tests
```bash
$ make test
```

#### 4. Make a report
```bash
$ make report
```
