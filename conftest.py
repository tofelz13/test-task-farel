from pytest import fixture
from pages.application import Application


@fixture(scope="session")
def app(request):
    link = request.config.getoption("--base-link")
    headless = request.config.getoption("--headless")
    username = request.config.getoption("--username")
    password = request.config.getoption("--password")
    url = "https://{}:{}@{}".format(username, password, link)
    app = Application(headless, url)
    yield app
    app.browser_close()


def pytest_addoption(parser):
    parser.addoption(
        "--base-link",
        action="store",
        default="qa-interview.farel.io",
        help="enter link",
    ),
    parser.addoption(
        "--username",
        action="store",
        default="qa",
        help="enter username",
    ),
    parser.addoption(
        "--password",
        action="store",
        default="sanfrancisco!",
        help="enter password",
    ),
    parser.addoption(
        "--headless",
        action="store",
        default=True,
        help="launching browser without gui",
    )
